﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IAssetFactory{

    public abstract GameObject LoadPlayer(string AssetName);
    public abstract GameObject LoadCollisionZone(string AssetName);
    public abstract GameObject LoadCollisionLine(string AssetName);
    public abstract GameObject LoadNote(string AssetName);
    public abstract GameObject LoadDirectionMark(string AssetName);
    public abstract Sprite LoadNoteSprite(NoteType noteType);

}
