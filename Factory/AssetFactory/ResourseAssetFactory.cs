﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourseAssetFactory : IAssetFactory
{
    private Sprite[] m_SpriteArray = new Sprite[4];

    public ResourseAssetFactory()
    {
        InitSpriteData();
    }

    public void InitSpriteData()
    {
        NoteType tmpNoteType;
        for (int i = 1; i < (int)NoteType.End; i++)
        {
            tmpNoteType = (NoteType)i;
            m_SpriteArray[i - 1] = Resources.Load<Sprite>("Unit/Note/Image/" + tmpNoteType.ToString());
        }
    }

    public override GameObject LoadPlayer(string AssetName)
    {
        GameObject tmpPlayerGameObject = null;
        var tmpLoad = Resources.Load("Unit/Player/"+AssetName+"") as GameObject;
        tmpPlayerGameObject = GameObject.Instantiate(tmpLoad);
        return tmpPlayerGameObject;
    }

    public override GameObject LoadCollisionZone(string AssetName)
    {
        GameObject tmpCollisionZone = null;
        var tmpLoad = Resources.Load("Collision/"+AssetName+"") as GameObject;
        tmpCollisionZone = GameObject.Instantiate(tmpLoad);
        return tmpCollisionZone;
    }

    public override GameObject LoadCollisionLine(string AssetName)
    {
        GameObject tmpCollisionLine = null;
        var tmpLoad = Resources.Load("CollisionLine/"+AssetName+"") as GameObject;
        tmpCollisionLine = GameObject.Instantiate(tmpLoad);
        return tmpCollisionLine;
    }

    public override GameObject LoadNote(string AssetName)
    {
        GameObject tmpNoteGameObject = null;
        var tmpLoad = Resources.Load("Unit/Note/"+AssetName+"") as GameObject;
        tmpNoteGameObject = GameObject.Instantiate(tmpLoad);
        return tmpNoteGameObject;
    }

    public override GameObject LoadDirectionMark(string AssetName)
    {
        GameObject tmpDirectionMarkObject = null;
        var tmpLoad = Resources.Load("DirectionMark/"+AssetName+"") as GameObject;
        tmpDirectionMarkObject = GameObject.Instantiate(tmpLoad);
        return tmpDirectionMarkObject;
    }

    public override Sprite LoadNoteSprite(NoteType noteType)
    {
        Sprite tmpNoteSprite = null;
        tmpNoteSprite = m_SpriteArray[(int)noteType - 1];
        return tmpNoteSprite;
    }


}
