﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IDirectionMarkFactory
{
    public abstract DirectionMark CreatDirectionMark();
}
