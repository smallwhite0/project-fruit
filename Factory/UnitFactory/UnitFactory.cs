﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitFactory : IUnitFactory
{
    private UnitBuildSystem m_BuildDirector = new UnitBuildSystem(Project_Fruit.Instance);
    private AwakeNoteBuilder theAwakeNoteBuilder = new AwakeNoteBuilder();

    public override IPlayer CreatePlayer(Vector3 SpawnPosition)
    {
        PlayerBuilderParam PlayerParam = new PlayerBuilderParam();
        PlayerParam.NewUnit = new Player();
        PlayerParam.vSpawnPosition = SpawnPosition;
        PlayerParam.AssetName = PlayerParam.NewUnit.GetAssetName();

        PlayerBuilder thePlayerBuilder = new PlayerBuilder();
        thePlayerBuilder.SetBuildParam(PlayerParam);
        m_BuildDirector.UnitCreate(thePlayerBuilder);

        return PlayerParam.NewUnit as IPlayer;
    }
    
    public override INote CreateNote()
    {
        NoteBuilderParam NoteParam = new NoteBuilderParam();
        NoteParam.NewUnit = new Note();
        NoteParam.AssetName = NoteParam.NewUnit.GetAssetName();

        NoteBuilder theNoteBuilder = new NoteBuilder();
        theNoteBuilder.SetBuildParam(NoteParam);
        m_BuildDirector.UnitCreate(theNoteBuilder);

        return NoteParam.NewUnit as INote;
    }

    public override INote AwakeNote(AwakeNoteSettingParam theAwakeNoteSettingParam)
    {
        theAwakeNoteBuilder.SetBuildParam(theAwakeNoteSettingParam);
        m_BuildDirector.UnitCreate(theAwakeNoteBuilder);
        return theAwakeNoteSettingParam.NewUnit as INote;
    }
}
