﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IUnitFactory {

    public abstract IPlayer CreatePlayer(Vector3 SpawnPosition);
    public abstract INote CreateNote();
    public abstract INote AwakeNote(AwakeNoteSettingParam theAwakeNoteSettingParam);
}
