﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Project_FruitFactory
{
    public static IUnitFactory m_UnitFactory = null;
    public static IDirectionMarkFactory m_DirectionMarkFactory = null;
    public static IAssetFactory m_AssetFactory = null;

    public static IUnitFactory GetUnitFactory()
    {
        if (m_UnitFactory == null)
            m_UnitFactory = new UnitFactory();

        return m_UnitFactory;
    }

    public static IDirectionMarkFactory GetDirectionMarkFactory()
    {
        if (m_DirectionMarkFactory == null)
            m_DirectionMarkFactory = new DirectionMarkFactory();

        return m_DirectionMarkFactory;
    }

    public static IAssetFactory GetAssetFactory()
    {
        if (m_AssetFactory == null)
            m_AssetFactory = new ResourseAssetFactory();

        return m_AssetFactory;
    }

}
