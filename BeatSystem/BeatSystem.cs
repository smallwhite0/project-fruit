﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;

public class BeatSystem : IGameSystem
{
    private Thread timer_thread;

    private bool m_bSongPlaying = false;
    private float m_fCrotchet = 0f; //一拍的長度
    private float m_fPreSongPosition = 0f;
    private float m_fSongPosition = 0f;
    private float m_fSongPosInBeats = 0f;
    private float m_fAdvanceSpawnBeats = 2.0f; //數字越小Note跑越快
    //private float m_fDeterminBeats = 0f; //Note判定的時間
    private float m_fDelayStartBeats = 4.0f;

    private SongInfo m_SongInfo;
    //private string m_FilePath;
    private string m_sSongName;
    private float m_fBPM = 0f;
    private float m_fOffset = 0f;
    

    private int m_iInputIndex = 0;
    private int m_iSongIndex = 0;
    private int m_iNowBeatCount = 0;

    private float m_fLastProcess;

    private Vector3 m_vFirstSpawnPosition = new Vector3(0, 10f, 0);
    private Vector3 m_vFirstHitPosition = new Vector3(0, 0.8f, 0);

    private bool m_ThreadLoop = true;

    [DllImport("BASSASIOAUDIO", EntryPoint = "MixerAddNew")]
    private static extern void MixerAddNew(int ChannelNum, IntPtr SoundFilePath, bool isSoundStartPause); //C++ void* == C# IntPtr

    [DllImport("BASSASIOAUDIO", EntryPoint = "SetMusic")]
    private static extern void AudioSetMusic(IntPtr MusicFilePath);

    [DllImport("BASSASIOAUDIO", EntryPoint = "Initial")]
    private static extern void AudioInitial(int device, float ConfigBufferLen, float UpdatePeriod);

    [DllImport("BASSASIOAUDIO", EntryPoint = "PlayMusic")]
    private static extern void AudioPlayMusic(double delay, float UpdateLength, float BPM, float Offset);

    [DllImport("BASSASIOAUDIO", EntryPoint = "ASIO_Start")]
    private static extern void AudioASIO_Start(int BufferSize);

    [DllImport("BASSASIOAUDIO", EntryPoint = "PlaySoundEffect_ASIO")]
    private static extern void AudioPlaySoundEffect(int chanIndex);

    [DllImport("BASSASIOAUDIO", EntryPoint = "MusicResume")]
    private static extern void AudioMusicResume();

    [DllImport("BASSASIOAUDIO", EntryPoint = "MusicPause")]
    private static extern void AudioMusicPause();

    [DllImport("BASSASIOAUDIO", EntryPoint = "SetASIOVol")]
    private static extern void AudioSetASIOVol(float ASIOVol);

    [DllImport("BASSASIOAUDIO", EntryPoint = "SetSoundVol")]
    private static extern void AudioSetSoundVol(int chanIndex, float Vol);

    [DllImport("BASSASIOAUDIO", EntryPoint = "SetMusicVol")]
    private static extern void AudioSetMusicVol(float Vol);

    [DllImport("BASSASIOAUDIO", EntryPoint = "BASSUpdate")]
    private static extern void BASSUpdate();

    [DllImport("BASSASIOAUDIO", EntryPoint = "Release")]
    private static extern void AudioRelease();

    [DllImport("BASSASIOAUDIO", EntryPoint = "GetSongPosInBeats")]
    private static extern float AudioGetSongPosInBeats();

    [DllImport("BASSASIOAUDIO", EntryPoint = "GetSongPos")]
    private static extern float AudioGetSongPos();

    [DllImport("BASSASIOAUDIO", EntryPoint = "IsMusicReadyToStart")]
    private static extern bool IsMusicReadyToStart();

    [DllImport("BASSASIOAUDIO", EntryPoint = "GetASIOVERSION")]
    private static extern int GetASIOVERSION();


    private void Call()
    {
        while (m_ThreadLoop)
        {
            if (m_PF.IsGamePause())
            {
                //Thread.Sleep(5);
                continue;
            }
                

            BASSUpdate();

            m_fSongPosition = AudioGetSongPos();
            m_fSongPosInBeats = m_fSongPosition / m_fCrotchet;
            //Thread.Sleep(5);
        }
    }

    public BeatSystem(Project_Fruit PF) : base(PF)
    {
        Initialize();
    }

    public override void Initialize()
    {
        m_SongInfo = m_PF.GetSongInfo();
        m_sSongName = m_PF.GetSongName();

        Debug.LogFormat("Song name : {0}", m_sSongName);


        SetBPM(m_SongInfo.BPM);
        SetOffset(m_SongInfo.Offset);
        Debug.Log(m_fOffset);
        AudioInitial(1, 100f, 0f);

        IntPtr MusicPath = Marshal.StringToHGlobalAnsi(Application.dataPath + "/Resources/Song/" + m_sSongName + "/" + m_sSongName + "_clip" + ".mp3"); //string to IntPtr
        IntPtr SoundPath1 = Marshal.StringToHGlobalAnsi(Application.dataPath + "/Resources/Sound/" + "SoundEF1" + ".wav");
        IntPtr SoundPath2 = Marshal.StringToHGlobalAnsi(Application.dataPath + "/Resources/Sound/" + "SoundEF2" + ".wav");
        IntPtr SoundPath3 = Marshal.StringToHGlobalAnsi(Application.dataPath + "/Resources/Sound/" + "SoundEF3" + ".wav");

        AudioSetASIOVol(0.6f);
        AudioSetMusic(MusicPath);
        AudioSetMusicVol(0.07f);

        MixerAddNew(0, SoundPath1, true);
        AudioSetSoundVol(0, 0.2f);
        MixerAddNew(1, SoundPath2, true);
        AudioSetSoundVol(1, 0.2f);
        MixerAddNew(2, SoundPath3, true);
        AudioSetSoundVol(2, 0.2f);
    }


    public void MusicStart()
    {
        Debug.LogFormat("Note live beat : {0}", m_fAdvanceSpawnBeats);
        Debug.LogFormat("Note delay beat : {0}", m_fDelayStartBeats);
        AudioPlayMusic(m_fDelayStartBeats * m_fCrotchet, 20f, m_fBPM, m_fOffset); //plugin
        //timer_thread = new Thread(Call);
        //timer_thread.IsBackground = true;
        //timer_thread.Start();
        AudioASIO_Start(56);
        m_bSongPlaying = true;
    }

    public void PlaySoundEffect(int chanIndex)
    {
        AudioPlaySoundEffect(chanIndex);
    }

    public void SetBPM(float theBPM)
    {
        m_fBPM = theBPM;
        m_fCrotchet = 60f / m_fBPM;
    }

    public void SetOffset(float theOffset)
    {
        m_fOffset = theOffset;
    }

    public bool GetMusicReady()
    {
        return IsMusicReadyToStart();
    }

    public float GetCrotchet()
    {
        return m_fCrotchet;
    }

    public float GetSongPosition()
    {
        return m_fSongPosition;
    }

    public float GetSongPosInBeats()
    {
        return m_fSongPosInBeats;
    }

    public float GetPrepareSongPosInBeats()
    {
        return m_fAdvanceSpawnBeats + m_fSongPosInBeats;
    }

    /*
    public float CaculateBeatDropProcess(float theBeatOfNote)
    {
        //m_fLastProcess = ((m_fAdvanceSpawnBeats - (theBeatOfNote - m_fSongPosInBeats)) - m_fDeterminBeats) / m_fAdvanceSpawnBeats;
        m_fLastProcess = ((m_fAdvanceSpawnBeats - (theBeatOfNote - m_fSongPosInBeats))) / m_fAdvanceSpawnBeats;
        return m_fLastProcess;
    }
    */

    public Vector3 GetSpawnPos()
    {
        return m_vFirstSpawnPosition;
    }

    public Vector3 GetHitPos()
    {
        return m_vFirstHitPosition;
    }

    /*
    public float GetNoteLiveTime()
    {
        return m_fAdvanceSpawnBeats * m_fCrotchet;
    }
    */

    public float GetNoteLiveBeats()
    {
        return m_fAdvanceSpawnBeats;
    }

    public float GetDelayStartBeats()
    {
        return m_fDelayStartBeats;
    }

    public float GetTabsStartPosY()
    {
        return m_fDelayStartBeats * ((m_vFirstSpawnPosition.y - m_vFirstHitPosition.y) / m_fAdvanceSpawnBeats) + m_vFirstHitPosition.y; //最後要加上基準點
    }

    public float CaculatePosMoveY(float theDurationBeat)
    {
        return theDurationBeat * ((m_vFirstSpawnPosition.y - m_vFirstHitPosition.y) / m_fAdvanceSpawnBeats);
    }

    /*
    public Vector3 CaculateRemovePos(Vector3 SpawnPos, Vector3 HitPos)
    {
        Vector3 tmpPos = Vector3.zero;
        tmpPos.x = HitPos.x;
        tmpPos.y = HitPos.y;
        return tmpPos;
    }
    */

    public override void Update()
    {
        if (m_PF.IsGamePause())
        {
            if (m_bSongPlaying)
            {
                AudioMusicPause();
                m_bSongPlaying = false;
            }

            return;
        }

        if (!m_bSongPlaying)
        {
            AudioMusicResume();
            m_bSongPlaying = true;
        }

        
        BASSUpdate();

        m_fSongPosition = AudioGetSongPos();
        m_fSongPosInBeats = m_fSongPosition / m_fCrotchet;

        
        double Duration = m_fSongPosition - m_fPreSongPosition;
        string latency = Duration > 0.005 ? "no" : "yes";

        /*
        if (Duration < 0.005)
        {
            Debug.LogFormat("Now beats: {0}   Now position: {1}  Duration : {2}  {3}", m_fSongPosInBeats, m_fSongPosition, Duration, latency);
        }
        */

        //Debug.LogFormat("Now beats: {0}   Now position: {1}  Duration : {2}  DeltaTime : {3}  {4}", m_fSongPosInBeats, m_fSongPosition, Duration, Time.deltaTime, latency);

        m_fPreSongPosition = m_fSongPosition;
        
    }

    public override void FixedUpdate()
    {
        //if (!IsMusicReadyToStart())
        //    return;

        /*
        BASSUpdate();
        m_fSongPosition = AudioGetSongPos();
        m_fSongPosInBeats = m_fSongPosition / m_fCrotchet;

        double Duration = m_fSongPosition - m_fPreSongPosition;
        string latency = Duration > 0.005 ? "no" : "yes";

        Debug.LogFormat("Now beats: {0}   Now position: {1}  Duration : {2}  {3}", m_fSongPosInBeats, m_fSongPosition, Duration, latency);

        m_fPreSongPosition = m_fSongPosition;
        */
    }

    /*
    public override void ApplicationFocus(bool focus)
    {
        isPause = !focus;
    }

    public override void ApplicationPause(bool pause)
    {
        isPause = pause;
    }
    */

    public override void Release()
    {
        if (timer_thread != null && timer_thread.IsAlive)
        {
            m_ThreadLoop = false;
            timer_thread.Join(1000);
        }
            
        AudioRelease();
    }
}
