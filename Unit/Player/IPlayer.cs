﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class IPlayer : IUnit
{
    public virtual void SetPlayerDirectionMark(IDirectionMark theDirectionMark) { }
    public virtual void InputControlMove() { }
    public virtual void InputControlColor() { }
}
