﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : IPlayer
{

    private SpriteRenderer m_SpriteRenderer = null;
    private DirectionMark m_DirectionMark = null;
    private GameObject m_CollisionZone = null;
    private GameObject m_CollisionLine = null;
    

    public Player()
    {
        m_AssetName = "Player";
    }

    public override void Initialize()
    {
        m_bKilled = false;
        m_SpriteRenderer = m_GameObject.transform.GetComponent<SpriteRenderer>();
        IAssetFactory theAssetFactory = Project_FruitFactory.GetAssetFactory();

        m_CollisionZone = theAssetFactory.LoadCollisionZone("CollisionZone");
        m_CollisionZone.transform.position = new Vector3(1.5f, 0, 0);
        m_CollisionZone.tag = "2";

        m_CollisionLine = theAssetFactory.LoadCollisionLine("CollisionLine");
        m_CollisionLine.transform.position = new Vector3(1.530892f, 0.8f, 0);
    }

    public override void SetPlayerDirectionMark(IDirectionMark theDirectionMark)
    {
        m_DirectionMark = theDirectionMark as DirectionMark;
    }

    public override void InputControlMove()
    {
        int iTmpTag = int.Parse(m_CollisionZone.tag);
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            iTmpTag = iTmpTag + 1;

            if (iTmpTag <= 3)
                m_GameObject.transform.position = m_GameObject.transform.position + new Vector3(1.5f, 0, 0);

            else
                iTmpTag = 3;

        }

        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            iTmpTag = iTmpTag - 1;

            if (iTmpTag >= 1)
                m_GameObject.transform.position = m_GameObject.transform.position - new Vector3(1.5f, 0, 0);

            else
                iTmpTag = 1;

        }

        m_CollisionZone.tag = iTmpTag.ToString();
    }

    public override void InputControlColor()
    {
        if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.DownArrow))
        {
            m_SpriteRenderer.color = Color.red;
            m_CollisionLine.GetComponent<SpriteRenderer>().color = Color.red;
            m_CollisionZone.layer = 10;
        }

        else
        {
            m_SpriteRenderer.color = Color.green;
            m_CollisionLine.GetComponent<SpriteRenderer>().color = Color.green;
            m_CollisionZone.layer = 9;
        }
    }

    #region Update
    public override void Update()
    {
        if (m_GameObject == null || m_CollisionZone == null)
            return;

        InputControlMove();
        InputControlColor();
    }
    #endregion
}
