﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note : INote
{
    public Note()
    {
        m_AssetName = "Note";
    }

    public override void Initialize()
    {
        m_bKilled = false;
    }

    
    public override void Update()
    {
        //Move();
    }
    
    public override void Release()
    {
        GameObject.Destroy(m_GameObject);
    }

    public override void UnitGetHit()
    {
        m_BeatSystem.PlaySoundEffect(m_SoundIndex);
        //Debug.LogFormat("PosY : {0}   Beat : {1}", m_GameObject.transform.position.y, m_BeatSystem.GetSongPosInBeats().ToString());
    }

    /*
    public override void UnitHitOtherOne()
    {
        //m_GameObject.GetComponent<SpriteRenderer>().enabled = false;
        //m_GameObject.GetComponent<CircleCollider2D>().enabled = false;
    }
    */

    public override void GetOffComponents()
    {
        if (m_NoteTirgger != null)
        {
            m_NoteTirgger.ReleaseFromUnit();
            m_NoteTirgger = null;
        }

        /*
        if (m_Move != null)
        {
            m_Move.ReleaseFromUnit();
            m_Move = null;
        }
        */
    }

    public override void SetSoundEffect(int SoundIndex)
    {
        m_SoundIndex = SoundIndex;
    }
}
