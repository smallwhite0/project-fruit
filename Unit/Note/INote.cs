﻿using UnityEngine;

public enum NoteType
{
    Null,
    Green,
    NegativeGreen,
    Red,
    NegativeRed,
    End
}

public abstract class INote : IUnit
{
    protected static BeatSystem m_BeatSystem;
    protected INoteTirgger m_NoteTirgger = null;
    protected IMove m_Move = null;

    public static void SetBeatSystem(BeatSystem theBeatSystem)
    {
        m_BeatSystem = theBeatSystem;
    }

    public void SetNoteTrigger(INoteTirgger NoteTrigger)
    {
        m_NoteTirgger = NoteTrigger;
    }

    public void SetNoteMove(IMove NoteMove)
    {
        m_Move = NoteMove;
    }

    protected float m_BeatOfNote;
    protected int m_SoundIndex;
    public abstract void SetSoundEffect(int SoundIndex);
}
