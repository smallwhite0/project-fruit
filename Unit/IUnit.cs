﻿using UnityEngine;

public abstract class IUnit{

    //Unit part 
    protected string m_AssetName = "";
    protected GameObject m_GameObject = null;
    protected Transform m_UnitTransform = null;

    //Unit game state
    protected bool m_bKilled = false;

    public void SetGameObject(GameObject theGameObject)
    {
        m_GameObject = theGameObject;
        m_UnitTransform = theGameObject.transform;
    }

    public GameObject GetGameObject()
    {
        return m_GameObject;
    }

    public Transform GetTransform()
    {
        return m_UnitTransform;
    }

    public string GetAssetName()
    {
        return m_AssetName;
    }

    public void IsKilled()
    {
        m_bKilled = true;
    }

    public void IsBack()
    {
        m_bKilled = false;
    }

    public bool IsRemovable()
    {
        return m_bKilled;
    }

    public virtual void Initialize() { }
    public virtual void GetOffComponents() { }
    public abstract void Update();
    public virtual void FixUpdate() { }
    public virtual void Release() { }
    
    public virtual void UnitGetHit() { }
    public virtual void UnitHitOtherOne() { }
}
