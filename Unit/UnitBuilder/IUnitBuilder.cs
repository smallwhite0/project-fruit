﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IUnitBuilderParam
{
    public IUnit NewUnit = null;
    public Vector3 vSpawnPosition;
    public string AssetName;
}

public abstract class IUnitBuilder
{
    public virtual void SetBuildParam(IUnitBuilderParam theBuilderParam) { }
    public virtual void LoadAsset() { }
    public virtual void SetSprite() { } //for notes in the pool
    public virtual void AddDirectionMark() { } //for player
    public virtual void SetTrigger() { } //for notes
    public virtual void SetMove() { }
    public virtual void SetTag() { }
    public virtual void SetSoundEffect() { }
    public virtual void SetActive() { }
    public virtual void AddUnitSystem(Project_Fruit PF) { }
}
