﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AwakeNoteSettingParam : IUnitBuilderParam
{
    //public float fBeatOfNote;
    //public Vector3 vHitPosition;
    public NoteType noteType;
    public string sNoteTag;
}

public class AwakeNoteBuilder : IUnitBuilder
{
    private AwakeNoteSettingParam m_AwakeNoteSettingParam = null;

    public override void SetBuildParam(IUnitBuilderParam theBuilderParam)
    {
        m_AwakeNoteSettingParam = theBuilderParam as AwakeNoteSettingParam;
    }

    public override void SetSprite()
    {
        IAssetFactory theAssetFactory = Project_FruitFactory.GetAssetFactory();
        m_AwakeNoteSettingParam.NewUnit.GetGameObject().GetComponent<SpriteRenderer>().sprite = theAssetFactory.LoadNoteSprite(m_AwakeNoteSettingParam.noteType);
    }

    public override void SetTag()
    {
        m_AwakeNoteSettingParam.NewUnit.GetGameObject().tag = m_AwakeNoteSettingParam.sNoteTag;
    }

    public override void SetTrigger()
    {
        INoteTirgger tmpNoteTirgger = null;
        GameObject tmpNoteGameObject = m_AwakeNoteSettingParam.NewUnit.GetGameObject();

        switch (m_AwakeNoteSettingParam.noteType)
        {
            case NoteType.Green:
                tmpNoteTirgger = tmpNoteGameObject.AddComponent<NoteTrigger>();
                tmpNoteGameObject.layer = 9;
                break;
            case NoteType.NegativeGreen:
                tmpNoteTirgger = tmpNoteGameObject.AddComponent<NegativeNoteTrigger>();
                tmpNoteGameObject.layer = 9;
                break;
            case NoteType.Red:
                tmpNoteTirgger = tmpNoteGameObject.AddComponent<NoteTrigger>();
                tmpNoteGameObject.layer = 10;
                break;
            case NoteType.NegativeRed:
                tmpNoteTirgger = tmpNoteGameObject.AddComponent<NegativeNoteTrigger>();
                tmpNoteGameObject.layer = 10;
                break;
        }

        tmpNoteTirgger.SetUnit(m_AwakeNoteSettingParam.NewUnit);
        tmpNoteTirgger.SetTagAndLayer(m_AwakeNoteSettingParam.sNoteTag, tmpNoteGameObject.layer);
        tmpNoteGameObject.layer = 12;
        (m_AwakeNoteSettingParam.NewUnit as INote).SetNoteTrigger(tmpNoteTirgger);
    }

    public override void SetSoundEffect()
    {
        int tmpSoundIndex;

        switch (m_AwakeNoteSettingParam.noteType)
        {

            case NoteType.Green:
                tmpSoundIndex = 0;
                break;
            case NoteType.NegativeGreen:
                tmpSoundIndex = 2;
                break;
            case NoteType.Red:
                tmpSoundIndex = 1;
                break;
            case NoteType.NegativeRed:
                tmpSoundIndex = 2;
                break;
            default:
                tmpSoundIndex = 0;
                break;
        }

        (m_AwakeNoteSettingParam.NewUnit as Note).SetSoundEffect(tmpSoundIndex);
    }

    public override void SetActive()
    {
        m_AwakeNoteSettingParam.NewUnit.IsBack();
        //m_AwakeNoteSettingParam.NewUnit.GetGameObject().transform.position = m_AwakeNoteSettingParam.vSpawnPosition;
        m_AwakeNoteSettingParam.NewUnit.GetGameObject().transform.localPosition = m_AwakeNoteSettingParam.vSpawnPosition;
        m_AwakeNoteSettingParam.NewUnit.GetGameObject().SetActive(true);
    }

    public override void AddUnitSystem(Project_Fruit PF)
    {
        PF.AddNote(m_AwakeNoteSettingParam.NewUnit as INote);
    }

}
