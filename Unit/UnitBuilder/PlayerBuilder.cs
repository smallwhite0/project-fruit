﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBuilderParam : IUnitBuilderParam
{
    //public int ArmorID;
}

public class PlayerBuilder : IUnitBuilder {

    private PlayerBuilderParam m_PlayerBuilderParam = null;

    public override void SetBuildParam(IUnitBuilderParam theBuilderParam)
    {
        m_PlayerBuilderParam = theBuilderParam as PlayerBuilderParam;
    }

    public override void LoadAsset()
    {
        IAssetFactory theAssetFactory = Project_FruitFactory.GetAssetFactory();
        GameObject PlayerGameObject = theAssetFactory.LoadPlayer(m_PlayerBuilderParam.AssetName);
        PlayerGameObject.transform.position = m_PlayerBuilderParam.vSpawnPosition;
        m_PlayerBuilderParam.NewUnit.SetGameObject(PlayerGameObject);
    }

    public override void AddDirectionMark()
    {
        IAssetFactory theAssetFactory = Project_FruitFactory.GetAssetFactory();
        IDirectionMarkFactory theDirectionMarkFactory = Project_FruitFactory.GetDirectionMarkFactory();
        DirectionMark tmpDirectionMark = null;
        GameObject tmpDirectionMarkObject = null;
        tmpDirectionMark = theDirectionMarkFactory.CreatDirectionMark();
        (m_PlayerBuilderParam.NewUnit as IPlayer).SetPlayerDirectionMark(tmpDirectionMark);
        tmpDirectionMark.SetOwnerTransform(m_PlayerBuilderParam.NewUnit.GetGameObject().transform);
        //tmpDirectionMark.SetDirectionMark();
        tmpDirectionMarkObject = theAssetFactory.LoadDirectionMark("DirectionMark");
        tmpDirectionMarkObject.transform.SetParent(m_PlayerBuilderParam.NewUnit.GetGameObject().transform);
        tmpDirectionMarkObject.transform.localPosition = new Vector3(0, 0.085f, -1);
        tmpDirectionMarkObject.transform.localScale = new Vector3(0.2f, 0.2f, 1);

    }

    public override void AddUnitSystem(Project_Fruit PF)
    {
        (m_PlayerBuilderParam.NewUnit as IPlayer).Initialize();
        PF.SetPlayer(m_PlayerBuilderParam.NewUnit as IPlayer);
    }
}
