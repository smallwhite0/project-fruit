﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteBuilderParam : IUnitBuilderParam
{
    //public float myBeatOfNote;
    //public Vector3 HitPosition;
    //public NoteType myNoteType;
}

public class NoteBuilder : IUnitBuilder
{
    private NoteBuilderParam m_NoteBuilderParam = null;

    public override void SetBuildParam(IUnitBuilderParam theBuilderParam)
    {
        m_NoteBuilderParam = theBuilderParam as NoteBuilderParam;
    }

    public override void LoadAsset()
    {
        IAssetFactory theAssetFactory = Project_FruitFactory.GetAssetFactory();
        GameObject NoteObject = theAssetFactory.LoadNote(m_NoteBuilderParam.AssetName);
        m_NoteBuilderParam.NewUnit.SetGameObject(NoteObject);
    }
}
