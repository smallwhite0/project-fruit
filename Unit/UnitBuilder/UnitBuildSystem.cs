﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitBuildSystem : IGameSystem {

    //Builder's Director
    public UnitBuildSystem(Project_Fruit PF) : base(PF) { }

    public void UnitCreate(IUnitBuilder theBuilder)
    {
        theBuilder.LoadAsset();
        theBuilder.SetSprite();
        theBuilder.AddDirectionMark();
        theBuilder.SetTag();
        theBuilder.SetMove();
        theBuilder.SetTrigger();
        theBuilder.SetSoundEffect();
        theBuilder.SetActive();
        theBuilder.AddUnitSystem(m_PF);
    }

}
