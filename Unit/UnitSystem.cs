﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSystem : IGameSystem
{
    private NotePool m_NotePool = null;
    private List<INote> m_Notes = new List<INote>();
    private IPlayer m_Player = null;

    public UnitSystem(Project_Fruit PF) : base(PF)
    {
        Initialize();
    }

    public override void Initialize()
    {
        m_NotePool = NotePool.Instance;
        m_NotePool.Initialize();
    }

    #region Unit
    public void AddNote(INote theNote)
    {
        m_Notes.Add(theNote);
    }

    public void RemoveNote(INote theNote)
    {
        m_Notes.Remove(theNote);
    }

    public void SetPlayer(IPlayer thePlayer)
    {
        if (m_Player != null)
            m_Player.Release();

        m_Player = thePlayer;
    }

    public void ReleasePlayer()
    {
        m_Player.Release();
    }
    #endregion

    #region Updating
    public override void Update()
    {
        UpdateUnit();
        RemoveUnit();
    }

    public void UpdateUnit()
    {
        if (m_Notes != null)
        {
            foreach (INote theNoteUnit in m_Notes)
            {
                theNoteUnit.Update();
            }
                
        }

        if (m_Player != null)
            m_Player.Update();
    }

    public void RemoveUnit()
    {
        List<IUnit> CanRemoveNotes = new List<IUnit>();

        foreach (INote theNote in m_Notes)
        {
            if (theNote.IsRemovable())
            {
                CanRemoveNotes.Add(theNote);
            }
        }

        foreach (INote theNote in CanRemoveNotes)
        {
            m_NotePool.EnqueueNoteToPool(theNote);
            m_Notes.Remove(theNote);
            theNote.GetOffComponents();
            theNote.GetGameObject().SetActive(false);
        }
    }

    public override void FixedUpdate(){}
    #endregion

}
