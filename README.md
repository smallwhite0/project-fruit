# Project Fruit  
![](Video/Demo.mp4)
Project Fruit是一款落下式的音樂遊戲，其靈感來自於音樂遊戲**OSU!**的**Catch the beat**模式和Konami的音樂遊戲。  

# 遊玩方式
伴隨著音樂節奏，接住配合節拍落下的Note(也就是遊戲中的水果)就可得分，玩家可利用鍵盤的方向左和方向右移動位置來接Note。
遊戲目前分成兩種顏色的Note，綠色和紅色，玩家在一般的狀態下(玩家顏色為綠色)可以接住綠色的Note。想要接住紅色的Note，必須按住空白鍵(玩家顏色變為紅色)才可成功。

除此之外，每一種顏色的Note都有另一種由藍色圓圈圍繞的變種，看到他們玩家必須閃躲這些Note，並且在碰到判定線之前變為相對應的顏色才算閃躲成功，否則會被扣分。

# 疑難雜症一 "回饋聲延遲"  
為了讓玩家接到Note的瞬間有感，在成功接住Note的瞬間加入了回饋音效，然而任何藉由Unity發出的遊戲音效，在Windows的環境下都會有延遲，尤其在音樂遊戲的要求下其延遲會特別明顯。

要解決這樣的問題，必須繞過Unity並且不使用Windows的內建音效驅動才可改善，而ASIO(Audio stream input output)便是可以繞過Windows的掌控，讓聲音延遲大幅降低的方案。
由於我能力低下，不會使用官方給的ASIO SDK，所以找到了BASS Audio Library裡的BASSASIO Library使用，有興趣可以到他們的官網 http://www.un4seen.com/ 觀看文件研究。

# 疑難雜症二 "譜面位置不一"
每次遊戲進行時，機率性會發生Note之間的間距會有所不同，由於一開始設計Note的生成位置並沒有一個參考點，
而是到了特定的歌曲時間後便在固定的位置所生成，如此一來容易受到frame drop的問題影響。   

順著這個想法，我把所有生出的Note全部放在一個基準點物件下當作他的子物件，並且依照他們對應的拍子算出他們離基準點多遠當作他們的local Position，最後只要讓基準點隨著歌曲位置移動，就可
以拉動所有的Note一起移動，且所有的Note都在他們應該所在的位置上。

# 疑難雜症三 "Note與音樂同步 Time.deltatime? SongPosition?"
音樂遊戲中Note與歌曲的同步一直是最核心的問題，為了要達成同步，你所選的參考時間軸將會至關重大。一開始會選用Unity的Time.deltatime當作歌曲每一個frame所經過的時間是非常直覺的，紀錄歌曲開始的時間後
便開始讓Note生出、下落至判定線。然而經過一段時間後，你便會發現所有的Note和音樂已經搭不起來，這是因為歌曲在每一個frame所經過的時間和Time.deltatime相同這個假設是錯誤的，兩者間會有相當微小的不同，
但經過長時間累積後會讓整個遊戲無法遊玩。

我便將參考時間軸轉至unity的AudioSettings.dspTime，是由unity的audio system專門計算歌曲實際經過的時間，以此為基準後便不會有Note和歌曲不同步的問題發生。但使用AudioSettings.dspTime會有另外一個問題，
就是Note在落下時會有上下抖動的現象發生，在每一個frame把AudioSettings.dspTime的值抓出來看之後發現，AudioSettings.dspTime的值會呈現2、2、2、7、9、10、10、15、15、18、20這樣的數列, 這些重複的數字導致
Note的移動十分的不自然。

決定不使用unity的audio system之後，我便把歌曲的播放交給了上述的BASSASIO Library處理，然而在抓取歌曲時間位置時一樣遇到了AudioSettings.dspTime的問題。最後把歌曲的播放交給BASS Audio Library，就可獲得
準確的歌曲時間位置，使得所有Note的落下過程十分的順暢，而BASSASIO Library負責需要極低延遲的回饋聲播放即可。