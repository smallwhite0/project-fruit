﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NegativeNoteTrigger : INoteTirgger
{

    private Collider2D[] Colliders = new Collider2D[4];
    private int CollidionNum = 0;

    /*
    private void OnTriggerEnter2D(Collider2D collision)
    {
        m_iLayer = m_Unit.GetGameObject().layer;
        m_sTag = m_Unit.GetGameObject().tag;

        if (collision.gameObject.layer == m_iLayer && collision.gameObject.tag != m_sTag) //閃避Note成功
        {
            m_Unit.IsKilled();
            //m_Unit.UnitGetHit();
            //Debug.Log(this.transform.position);
            //Debug.Log(m_BeatSystem.GetSongPosInBeats().ToString());
        }

        else //閃避Note失敗
        {
            m_bTrigger = true;
            m_Unit.UnitGetHit();
            m_Unit.IsKilled();
        }

    }
    */

    private void FixedUpdate()
    {
        CollidionNum = Physics2D.OverlapCircleNonAlloc(transform.position, 0.3f, Colliders, m_iMask);
        if (CollidionNum != 0)
        {
            if (Colliders[0].gameObject.layer == m_iLayer && Colliders[0].gameObject.tag != m_sTag) //閃避Note成功
            {
                m_Unit.IsKilled();
                //Debug.LogFormat("Position : {0}   Beat : {1}", m_Unit.GetGameObject().transform.position, m_BeatSystem.GetSongPosInBeats().ToString());
            }

            else //閃避Note失敗
            {
                m_bTrigger = true;
                m_Unit.UnitGetHit();
                m_Unit.IsKilled();
            }
        }
    }

    public override void ReleaseFromUnit()
    {
        if (m_bTrigger)
        {
            switch (m_iLayer)
            {
                case 9: //Layer 9 is green Layer
                    m_ScoreSystem.HitByGreen();
                    break;
                case 10: //Layer 10 is red Layer
                    m_ScoreSystem.HitByRed();
                    break;
            }
        }

        NegativeNoteTrigger tmpNegativeNoteTriggerComponent = m_Unit.GetGameObject().GetComponent<NegativeNoteTrigger>();
        if (tmpNegativeNoteTriggerComponent != null)
            Destroy(tmpNegativeNoteTriggerComponent);
    }

}
