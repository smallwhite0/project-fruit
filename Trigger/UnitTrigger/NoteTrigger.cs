﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteTrigger : INoteTirgger
{
    private Collider2D[] Colliders = new Collider2D[4];
    private int CollidionNum = 0;

    /*
    private void OnTriggerEnter2D(Collider2D collision)
    {
        m_iLayer = m_Unit.GetGameObject().layer;
        m_sTag = m_Unit.GetGameObject().tag;

        if (collision.gameObject.layer == m_iLayer && collision.gameObject.tag == m_sTag) //SpawnMark layer
        {
            m_Unit.IsKilled();
            m_Unit.UnitGetHit();
            m_bTrigger = true;
            Debug.LogFormat("Position : {0}   Beat : {1}", m_Unit.GetGameObject().transform.position, m_BeatSystem.GetSongPosInBeats().ToString());
        }

        else if (collision.gameObject.layer == 11)
        {
            m_Unit.IsKilled();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer == m_iLayer && collision.gameObject.tag == m_sTag) //SpawnMark layer
        {
            m_Unit.IsKilled();
            m_Unit.UnitGetHit();
            m_bTrigger = true;
            //Debug.Log(this.transform.position);
            //Debug.Log(m_BeatSystem.GetSongPosInBeats().ToString());
        }

    }
    */

    private void FixedUpdate()
    {
        CollidionNum = Physics2D.OverlapCircleNonAlloc(transform.position, 0.3f, Colliders, m_iMask);
        if (CollidionNum != 0)
        {
            if (Colliders[0].gameObject.layer == m_iLayer && Colliders[0].gameObject.tag == m_sTag)
            {
                m_Unit.IsKilled();
                m_Unit.UnitGetHit();
                m_bTrigger = true;
                //Debug.LogFormat("Position : {0}   Beat : {1}", m_Unit.GetGameObject().transform.position, m_BeatSystem.GetSongPosInBeats().ToString());
            }

            else if (Colliders[0].gameObject.layer == 11)
            {
                m_Unit.IsKilled();
            }
        }
    }

    public override void ReleaseFromUnit()
    {
        if (m_bTrigger)
        {
            switch (m_iLayer)
            {
                case 9: //Layer 9 is green Layer
                    m_ScoreSystem.PlusGreen();
                    break;
                case 10: //Layer 10 is red Layer
                    m_ScoreSystem.PlusRed();
                    break;
            }
        }

        else
        {
            switch (m_iLayer)
            {
                case 9: //Layer 9 is green Layer
                    m_ScoreSystem.MissGreen();
                    break;
                case 10: //Layer 10 is red Layer
                    m_ScoreSystem.MissRed();
                    break;
            }
        }

        NoteTrigger tmpNoteTriggerComponent = m_Unit.GetGameObject().GetComponent<NoteTrigger>();
        if (tmpNoteTriggerComponent != null)
            Destroy(tmpNoteTriggerComponent);   
    }

}
