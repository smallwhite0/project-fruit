﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class INoteTirgger : IUnitTrigger
{
    protected static BeatSystem m_BeatSystem = null;
    protected static ScoreSystem m_ScoreSystem = null;
    protected bool m_bTrigger = false;
    protected string m_sTag;
    protected int m_iLayer;
    protected int m_iMask;
    
    public void SetTagAndLayer(string theTag, int theLayer)
    {
        m_sTag = theTag;
        m_iLayer = theLayer;
        m_iMask = (1 << 9) | (1 << 10) | (1 << 11); //9 -> Green  10 -> Red  11 -> Release
    }

    public static void SetBeatSystem(BeatSystem theBeatSystem)
    {
        m_BeatSystem = theBeatSystem;
    }
    

    public static void SetScoreSystem(ScoreSystem theScoreSystem)
    {
        m_ScoreSystem = theScoreSystem;
    }

    public abstract void ReleaseFromUnit();
}
