﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IUnitTrigger : MonoBehaviour {

    protected IUnit m_Unit;

    public void SetUnit(IUnit theUnit)
    {
        m_Unit = theUnit;
    }

}
