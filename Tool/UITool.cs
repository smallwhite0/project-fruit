﻿using UnityEngine;

public static class UITool {

    private static GameObject m_CanvasObject = null;

    public static GameObject FindUIGameObject( string UIName )
    {
        
        if (m_CanvasObject == null)
            m_CanvasObject = UnityTool.FindGameObjecct("Canvas");
        if (m_CanvasObject == null)
            return null;

        return UnityTool.FindChildGameObject(m_CanvasObject, UIName);
    }

    public static T GetUIComponent<T>( GameObject Container, string UIname ) where T : UnityEngine.Component
    {
        GameObject p_TargetChildGameObject = UnityTool.FindChildGameObject(Container, UIname);

        if (p_TargetChildGameObject == null)
            return null;

        T p_TmpComponent = p_TargetChildGameObject.GetComponent<T>();

        if (p_TmpComponent == null)
        {
            Debug.LogError("Component [" + UIname + "] is not [" + typeof(T) + "]");
            return null;
        }

        return p_TmpComponent;
    }

}
