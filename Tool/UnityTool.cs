﻿using UnityEngine;

public static class UnityTool {

	public static GameObject FindGameObjecct( string GameObjectName )
    {
        GameObject p_TargetGameObject =  GameObject.Find(GameObjectName);

        if (p_TargetGameObject == null)
        {
            Debug.LogWarning("Can't find Gameobject [" + GameObjectName + "]");

            return null;
        }

        return p_TargetGameObject;
    }


    public static GameObject FindChildGameObject( GameObject Container, string ChildName)
    {

        if(Container == null)
        {
            Debug.LogError("Container is null");
            return null;
        }

        Transform p_ChildGameObjectTF = null;

        if (Container.name == ChildName)   //Container is the Target Child
            p_ChildGameObjectTF = Container.transform;


        else
        {
            Transform[] AllChildTF = Container.transform.GetComponentsInChildren<Transform>();

            foreach (Transform childTF in AllChildTF)
            {
                if (childTF.name == ChildName)
                {
                    if (p_ChildGameObjectTF == null)
                        p_ChildGameObjectTF = childTF;

                    else    // has a repeat name
                        Debug.LogWarning("Repeat GameObject Name in ["+ Container .name+ "] : "+ ChildName + " ");
                    
                }
            }

        }


        if (p_ChildGameObjectTF == null) // no such child
        {
            Debug.LogError("Can't find Child Gameobject [" + ChildName + "]");
            return null;
        }

        return p_ChildGameObjectTF.gameObject;
    }
}
