﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneStateManager
{

    private ISceneState m_State;
    private bool m_bRunBegin = false;
    private string m_sLoadSceneName = "";

    public SceneStateManager() { }

    public void SetState(ISceneState State, string LoadSceneName)
    {
        m_bRunBegin = false;

        LoadScene(LoadSceneName);

        m_sLoadSceneName = LoadSceneName;
        if (m_sLoadSceneName == "")
            m_sLoadSceneName = "StartScene";

        if (m_State != null)
            m_State.StateEnd();

        m_State = State;
    }

    private void LoadScene(string LoadSceneName)
    {
        if (LoadSceneName == null || LoadSceneName.Length == 0)
            return;

        SceneManager.LoadScene(LoadSceneName, LoadSceneMode.Single);

    }

    public void StateUpdate()
    {
        //Not loading yet
        if (!SceneManager.GetSceneByName(m_sLoadSceneName).isLoaded)
            return;

        if (m_State != null && m_bRunBegin == false)
        {
            m_State.StateBegin();
            m_bRunBegin = true;
        }

        if (m_State != null)
            m_State.StateUpdate();
    }

    public void StateFixedUpdate()
    {
        //Not loading yet
        if (!SceneManager.GetSceneByName(m_sLoadSceneName).isLoaded)
            return;

        if (m_State != null && m_bRunBegin == false)
            return;

        if (m_State != null)
            m_State.StateFixedUpdate();
    }

    public void ApplicationFocus(bool focus)
    {
        if (m_State != null)
            m_State.ApplicationFocus(focus);
    }

    public void ApplicationPause(bool pause)
    {
        if (m_State != null)
            m_State.ApplicationPause(pause);
    }

    public void StateRelease()
    {
        if (m_State != null)
            m_State.StateEnd();
    }
}