﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuState : ISceneState{

    public MainMenuState(SceneStateManager Manager) : base(Manager)
    {
        this.StateName = "MainMenuScene";
    }

    public override void StateBegin()
    {
        Debug.Log(this.StateName);
        GameObject tmpRoot = UITool.FindUIGameObject("GameStartUI");
        Button tmpBtn = UITool.GetUIComponent<Button>(tmpRoot ,"GameStartBtn");
        if (tmpBtn != null)
        {
            Debug.Log("Hello");
            tmpBtn.onClick.AddListener(() => OnGameStartButtonClick());
        }

    }

    public void OnGameStartButtonClick()
    {
        m_Manager.SetState(new SongSelectState(m_Manager), "SongSelectScene");
    }
}
