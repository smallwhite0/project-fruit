﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class NormalPlayState : ISceneState{

    private SelectSongData tmpSelectSongData;

    public NormalPlayState(SceneStateManager Manager, SelectSongData theSelectSongData) : base(Manager)
    {
        tmpSelectSongData = theSelectSongData;
        this.StateName = "PlayScene";
    }

    public override void StateBegin()
    {
        Project_Fruit.Instance.Initialize(tmpSelectSongData);
    }

    public override void StateUpdate()
    {        
        Project_Fruit.Instance.Update();
    }

    public override void StateFixedUpdate()
    {
        Project_Fruit.Instance.FixedUpdate();
    }

    public override void ApplicationFocus(bool focus)
    {
        Project_Fruit.Instance.ApplicationFocus(focus);
    }

    public override void ApplicationPause(bool pause)
    {
        Project_Fruit.Instance.ApplicationPause(pause);
    }

    public override void StateEnd()
    {
        Project_Fruit.Instance.Release();
    }
}
