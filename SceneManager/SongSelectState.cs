﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class SongSelectState : ISceneState
{
    private SongSelectUICenter m_SongSelectUICenter;
    private SelectSongData m_SelectedSongData;

    public SongSelectState(SceneStateManager Manager) : base(Manager)
    {
        this.StateName = "SongSelectScene";
    }

    public override void StateBegin()
    {
        Debug.Log(this.StateName);
        GameObject tmpUIRoot = UITool.FindUIGameObject("SongSelectUI");
        m_SongSelectUICenter = tmpUIRoot.GetComponent<SongSelectUICenter>();
        m_SongSelectUICenter.SongSelectUIInitial(this);
    }

    public override void StateUpdate()
    {
        m_SongSelectUICenter.SongSelectUIUpdate();
    }

    public void OnSongSelectButtonClick(SelectSongBtnData theSelectSongBtnData)
    {
        m_SelectedSongData.sSelectedSongName = theSelectSongBtnData.GetSongName();
        m_Manager.SetState(new NormalPlayState(m_Manager, m_SelectedSongData), "PlayScene");
    }


}
