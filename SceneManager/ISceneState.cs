﻿using UnityEngine;

public abstract class ISceneState {

    protected SceneStateManager m_Manager = null;
    private string m_sStateName = "ISceneState";
    public string StateName
    {
        get { return m_sStateName; }
        set { m_sStateName = value; }
    }

    public ISceneState(SceneStateManager Manager)
    {
        m_Manager = Manager;
    }

    public virtual void StateBegin()
    { }

    public virtual void StateEnd()
    { }

    public virtual void StateUpdate()
    { }

    public virtual void StateFixedUpdate()
    { }

    public virtual void ApplicationFocus(bool focus)
    { }

    public virtual void ApplicationPause(bool pause)
    { }
}
