﻿using UnityEngine;

public class StartState : ISceneState{

    private int m_iTime = 0;

    public StartState(SceneStateManager Manager) : base(Manager)
    {
        this.StateName = "StartScene";
        m_iTime = 0;
    }

    public override void StateBegin()
    {
        Debug.Log(this.StateName);
    }

    public override void StateUpdate()
    {
        m_iTime++;
        if (m_iTime >= 1)
            m_Manager.SetState(new MainMenuState(m_Manager), "MainMenuScene");

        return;
    }
}
