﻿using System;
using UnityEngine;

public class Project_Fruit{

    private static Project_Fruit _instance = null;
    public static Project_Fruit Instance
    {
        get
        {
            if (_instance == null)
                _instance = new Project_Fruit();

            return _instance;
        }
    }

    private UnitSystem m_UnitSystem = null;
    private BeatSystem m_BeatSystem = null;
    private NoteSpawnSystem m_NoteSpawnSystem = null;
    private ScoreSystem m_ScoreSystem = null;
    private SelectSongData m_SelectSongData;
    private SongInfo m_SongInfo;
    private string m_sSongName;

    //private bool m_bGameover = false;
    //private int m_iGameMode = 0;

    private bool m_bGamePause = false;

    public void Initialize(SelectSongData theSelectSongData)
    {
        Application.targetFrameRate = 60;
        m_SelectSongData = theSelectSongData;
        m_sSongName = m_SelectSongData.sSelectedSongName;
        m_SongInfo = Resources.Load("Song/" + m_sSongName + "/" + m_sSongName + "") as SongInfo;
        m_UnitSystem = new UnitSystem(this);
        m_BeatSystem = new BeatSystem(this);
        INote.SetBeatSystem(m_BeatSystem);
        TabsMove.SetBeatSystem(m_BeatSystem);
        INoteTirgger.SetBeatSystem(m_BeatSystem);
        NoteSpawnSystem.SetBeatSystem(m_BeatSystem);
        m_NoteSpawnSystem = new NoteSpawnSystem(this);
        m_ScoreSystem = new ScoreSystem(this);
        INoteTirgger.SetScoreSystem(m_ScoreSystem);
        //NoteMove.SetBeatSystem(m_BeatSystem);
        
        InitialPlayer();
        m_BeatSystem.MusicStart();
    }

    public void InitialPlayer()
    {
        Vector3 vPlayerPosition = new Vector3(1.5f, 0.3f, 0);
        IUnitFactory theFactory = Project_FruitFactory.GetUnitFactory();
        theFactory.CreatePlayer(vPlayerPosition);
    }

    /*
    public void Reset()
    {

    }
    */

    public void Release()
    {
        m_UnitSystem.Release();
        m_BeatSystem.Release();
        m_NoteSpawnSystem.Release();
        m_ScoreSystem.Release();
    }

    public void Update()
    {
        if (m_UnitSystem != null)
            m_UnitSystem.Update();

        if (m_BeatSystem != null)
            m_BeatSystem.Update();

        if (m_NoteSpawnSystem != null)
            m_NoteSpawnSystem.Update();
    }

    public void FixedUpdate()
    {
        /*
        if (m_BeatSystem != null)
            m_BeatSystem.FixedUpdate();

        if (m_NoteSpawnSystem != null)
            m_NoteSpawnSystem.FixedUpdate();
        */
    }

    public void ApplicationFocus(bool focus)
    {
        m_bGamePause = !focus;
    }

    public void ApplicationPause(bool pause)
    {
        m_bGamePause = pause;
    }

    public void SetPlayer(IPlayer thePlayer)
    {
        m_UnitSystem.SetPlayer(thePlayer);
    }

    public void AddNote(INote theNote)
    {
        m_UnitSystem.AddNote(theNote);
    }

    public void SetBPM(float theBPM)
    {
        m_BeatSystem.SetBPM(theBPM);
    }

    public void SetOffset(float theOffset)
    {
        m_BeatSystem.SetOffset(theOffset);
    }

    public float GetPrepareSongPosInBeats()
    {
        return m_BeatSystem.GetPrepareSongPosInBeats();
    }

    public bool GetMusicReady()
    {
        return m_BeatSystem.GetMusicReady();
    }

    public SongInfo GetSongInfo()
    {
        return m_SongInfo;
    }

    public string GetSongName()
    {
        return m_sSongName;
    }

    public bool IsGamePause()
    {
        return m_bGamePause;
    }

}
