﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
public class DirectionMarkData
{
    public KeyCode[] KeyPadPress = new KeyCode[2];
    public float fDirectionMarkRotation = 0f;
    public bool bButtonPress = false;
    public string DirectionMarkTag = null;
}
*/

public class DirectionMark : IDirectionMark
{
    private GameObject m_DirectionMark = null;
    private Transform m_OwnerTransform = null;
    //private float m_fDirectionMarkRotation = 0f;
    //private float m_RotateStep = 0f;
    //private float m_fPreviousRotation = 0f;
    //private bool m_bKeyPadPress = false;
    //private DirectionMarkData[] m_DirectionMarkData = new DirectionMarkData[10];

    public DirectionMark()
    {
        Initialize();
    }

    public override void Initialize()
    {
        /*
        for (int i = 0; i < m_DirectionMarkData.Length; i++)
        {
            m_DirectionMarkData[i] = new DirectionMarkData();
        }

        m_DirectionMarkData[1].KeyPadPress[0] = KeyCode.Keypad1;
        m_DirectionMarkData[1].KeyPadPress[1] = KeyCode.X;
        m_DirectionMarkData[1].fDirectionMarkRotation = 135f;
        m_DirectionMarkData[1].DirectionMarkTag = "4";

        m_DirectionMarkData[2].KeyPadPress[0] = KeyCode.Keypad2;
        m_DirectionMarkData[2].KeyPadPress[1] = KeyCode.C;
        m_DirectionMarkData[2].fDirectionMarkRotation = 180f;
        m_DirectionMarkData[2].DirectionMarkTag = "5";

        m_DirectionMarkData[3].KeyPadPress[0] = KeyCode.Keypad3;
        m_DirectionMarkData[3].KeyPadPress[1] = KeyCode.V;
        m_DirectionMarkData[3].fDirectionMarkRotation = 225f;
        m_DirectionMarkData[3].DirectionMarkTag = "6";

        m_DirectionMarkData[4].KeyPadPress[0] = KeyCode.Keypad4;
        m_DirectionMarkData[4].KeyPadPress[1] = KeyCode.S;
        m_DirectionMarkData[4].fDirectionMarkRotation = 90f;
        m_DirectionMarkData[4].DirectionMarkTag = "3";

        m_DirectionMarkData[6].KeyPadPress[0] = KeyCode.Keypad6;
        m_DirectionMarkData[6].KeyPadPress[1] = KeyCode.F;
        m_DirectionMarkData[6].fDirectionMarkRotation = 270f;
        m_DirectionMarkData[6].DirectionMarkTag = "7";

        m_DirectionMarkData[7].KeyPadPress[0] = KeyCode.Keypad7;
        m_DirectionMarkData[7].KeyPadPress[1] = KeyCode.W;
        m_DirectionMarkData[7].fDirectionMarkRotation = 45f;
        m_DirectionMarkData[7].DirectionMarkTag = "2";

        m_DirectionMarkData[8].KeyPadPress[0] = KeyCode.Keypad8;
        m_DirectionMarkData[8].KeyPadPress[1] = KeyCode.E;
        m_DirectionMarkData[8].fDirectionMarkRotation = 0f;
        m_DirectionMarkData[8].DirectionMarkTag = "1";

        m_DirectionMarkData[9].KeyPadPress[0] = KeyCode.Keypad9;
        m_DirectionMarkData[9].KeyPadPress[1] = KeyCode.R;
        m_DirectionMarkData[9].fDirectionMarkRotation = 315f;
        m_DirectionMarkData[9].DirectionMarkTag = "8";
        */
    }

    public override void SetOwnerTransform(Transform thePlayerTransform)
    {
        m_OwnerTransform = thePlayerTransform;
    }

    public override void SetGameobject(GameObject directionMarkObject)
    {
        m_DirectionMark = directionMarkObject;
    }

    #region Mark旋轉or移動
    /*
    public void CheckKeyPadPress()
    {
        
        foreach (DirectionMarkData MarkData in m_DirectionMarkData)
        {
            if (Input.GetKeyDown(MarkData.KeyPadPress[0]) || Input.GetKeyDown(MarkData.KeyPadPress[1]))
                MarkData.bButtonPress = true;

            else
                MarkData.bButtonPress = false;
        }
        
    }

    public override void DirectionMarkRotate()
    {
        CheckKeyPadPress();

        foreach (DirectionMarkData MarkData in m_DirectionMarkData)
        {
            if (!MarkData.bButtonPress)
                continue;

            m_fDirectionMarkRotation = MarkData.fDirectionMarkRotation;
            m_DirectionMark.transform.RotateAround(m_OwnerTransform.position, Vector3.forward, m_fDirectionMarkRotation - m_fPreviousRotation);
            m_fPreviousRotation = m_fDirectionMarkRotation;
            m_OwnerTransform.tag = MarkData.DirectionMarkTag;
        }
    }
    */
    #endregion

    public override void Release()
    {
        base.Release();
    }

}
