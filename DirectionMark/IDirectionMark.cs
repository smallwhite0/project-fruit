﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
public class Transform_Structure
{
    public Vector3    NewSpawnPosition = Vector3.zero;
    public Quaternion NewSpawnRotation = Quaternion.identity;
}
*/

public abstract class IDirectionMark{

    public virtual void SetOwnerTransform(Transform thePlayerTransform) { }
    public virtual void SetGameobject(GameObject directionMarkObject) { }
    public virtual void DirectionMarkRotate() { }
    public virtual void Initialize() { }
    public virtual void Release() { }
    public virtual void Update() { }

}
