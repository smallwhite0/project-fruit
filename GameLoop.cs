﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLoop : MonoBehaviour {

    private SceneStateManager m_SceneStateManager = new SceneStateManager();

    void Awake()
    {
        GameObject.DontDestroyOnLoad(this.gameObject);
    }

    // Use this for initialization
    void Start () {

        m_SceneStateManager.SetState(new StartState(m_SceneStateManager), "");
    }
	
	// Update is called once per frame
	void Update () {

        m_SceneStateManager.StateUpdate();

    }

    void FixedUpdate()
    {
        m_SceneStateManager.StateFixedUpdate();
    }

    private void OnApplicationFocus(bool focus)
    {
        m_SceneStateManager.ApplicationFocus(focus);
    }

    private void OnApplicationPause(bool pause)
    {
        m_SceneStateManager.ApplicationPause(pause);
    }

    private void OnDestroy()
    {
        m_SceneStateManager.StateRelease();
    }
}
