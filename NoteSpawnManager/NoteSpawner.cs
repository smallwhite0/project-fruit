﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteSpawner : INoteSpawner
{
    private NoteListData m_NoteListData = null;
    //private NotePool theNotePool = NotePool.Instance;

    public void SetNoteListData(NoteListData theNoteListData)
    {
        m_NoteListData = theNoteListData;
        //Debug.Log(m_NoteListData.NoteList.Count);
    }

    public NoteData GetCurrentNoteData()
    {
        int tmpIndex = m_NoteListData.CurrentIndex;
        if (tmpIndex == m_NoteListData.NoteList.Count)
            return null;

        return m_NoteListData.NoteList[tmpIndex];
    }

    public override void NoteStandby(float theBeat, float NoteLiveBeats, string theTag, Project_Fruit PF, NotePool theNotePool)
    {
        NoteData tmpNoteData = GetCurrentNoteData();
        Vector3 tmpSpawnPotision = Vector3.zero;
        tmpSpawnPotision.x = m_vNoteSpawnPotision.x;
        tmpSpawnPotision.y = theBeat * ((m_vNoteSpawnPotision.y - m_vNoteHitPotision.y) / NoteLiveBeats);

        //INote tmpNote = theNotePool.AwakeNoteByNewSets(theBeat, m_vNoteSpawnPotision, m_vNoteHitPotision, tmpNoteData.noteType, theTag, PF);
        INote tmpNote = theNotePool.AwakeNoteByNewSets(tmpSpawnPotision, m_vNoteHitPotision, tmpNoteData.noteType, theTag, PF);
        //Debug.LogFormat("{0} * {1} /  {2} = {3}", theBeat, m_vNoteSpawnPotision.y - m_vNoteHitPotision.y, NoteLiveBeats, tmpSpawnPotision.y);

        m_NoteListData.CurrentIndex++;
    }
}
