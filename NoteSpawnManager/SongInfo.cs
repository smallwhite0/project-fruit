﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NoteData : IComparable<NoteData>
{
    public float BeatOfSong = 0;
    public NoteType noteType = NoteType.Null;
    public int NoteAttrID = 0;
    public bool isSelected = false;

    public int CompareTo(int intNumber)
    {
        return this.BeatOfSong.CompareTo(intNumber);
    }

    public int CompareTo(NoteData otherNoteData)
    {
        if (otherNoteData == null)
            return 1;

        else
            return this.BeatOfSong.CompareTo(otherNoteData.BeatOfSong);
    }
}

[System.Serializable]
public class NoteListData
{
    public int CurrentIndex = 0;
    public List<NoteData> NoteList = new List<NoteData>();
}

public class SongInfo : ScriptableObject
{
    public float BPM = 0f;
    public float Offset = 0f;
    public NoteListData[] m_SongNoteListSet = new NoteListData[8];
    private bool isSongFinished = false;

    public void CleanSelected()
    {
        foreach (NoteListData theNoteListData in m_SongNoteListSet)
        {
            for (int i = 0; i < theNoteListData.NoteList.Count; i++)
                theNoteListData.NoteList[i].isSelected = false;

        }
    }

    public bool IsSongFinished()
    {
        return isSongFinished;
    }
}