﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteSpawnSystem : IGameSystem
{
    private static BeatSystem m_BeatSystem = null;
    private NoteSpawner[] m_NoteSpawnerSet;
    private SongInfo m_SongInfo = null;
    private int m_iFinishCount = 0;
    private float m_fStandbyBeat = 0f;
    private Vector3 m_vFirstSpawnPosition;
    private Vector3 m_vFirstHitPosition;
    private int m_iGameMode = 0;
    private NotePool m_NotePool;

    public NoteSpawnSystem(Project_Fruit PF) : base(PF)
    { 
        Initialize();
    }

    public override void Initialize()
    {
        m_vFirstSpawnPosition = m_BeatSystem.GetSpawnPos();
        m_vFirstHitPosition = m_BeatSystem.GetHitPos();

        switch (m_iGameMode)
        {
            case 0:
                m_NoteSpawnerSet = new NoteSpawner[3];
                break;
            case 1:
                m_NoteSpawnerSet = new NoteSpawner[8];
                break;
        }

        for (int i = 0; i < m_NoteSpawnerSet.Length; i++)
        {
            if (m_NoteSpawnerSet[i] != null)
                continue;

            m_NoteSpawnerSet[i] = new NoteSpawner();
        }
        SetSpawnPotision();
        SetSongInfo(m_PF.GetSongInfo());

        //Set Tabs Move
        m_NotePool = NotePool.Instance;
        m_NotePool.SetTabsStartPosY(m_BeatSystem.GetTabsStartPosY());
        //Debug.LogFormat("Pool Pos : {0}   PosY : {1}", m_NotePool.GetGameObject().transform.position, m_BeatSystem.GetTabsStartPosY());

        TabsMove tmpTabsMove = m_NotePool.GetGameObject().AddComponent<TabsMove>();
        tmpTabsMove.SetNotePosition();
        tmpTabsMove.SetLastBeat(-m_BeatSystem.GetDelayStartBeats());
        tmpTabsMove.SetGameObject(m_NotePool.GetGameObject());
    }

    public static void SetBeatSystem(BeatSystem theBeatSystem)
    {
        m_BeatSystem = theBeatSystem;
    }

    public void SetSongInfo(SongInfo theSongInfo)
    {
        m_SongInfo = theSongInfo;
        for (int i = 0; i < m_NoteSpawnerSet.Length; i++)
        {
            m_SongInfo.m_SongNoteListSet[i].CurrentIndex = 0;
            m_NoteSpawnerSet[i].SetNoteListData(m_SongInfo.m_SongNoteListSet[i]);
        }
    }

    public void SetSpawnPotision()
    {
        if (m_iGameMode == 0) //接水果
        {
            for (int i = 0; i < m_NoteSpawnerSet.Length; i++)
            {
                m_NoteSpawnerSet[i].SetNoteSpawnPotision(m_vFirstSpawnPosition + new Vector3(1.5f * i, 0, 0));
                m_NoteSpawnerSet[i].SetNoteHitPotision(m_vFirstHitPosition + new Vector3(1.5f * i, 0, 0));
            }
        }

        else if (m_iGameMode == 1) //一般
        {
            for (int i = 0; i < m_NoteSpawnerSet.Length; i++)
            {
                Debug.Log((Quaternion.Euler(0, 0, 45 * i) * m_vFirstSpawnPosition).ToString("f4"));
                m_NoteSpawnerSet[i].SetNoteSpawnPotision(Quaternion.Euler(0, 0, 45 * i) * m_vFirstSpawnPosition);
                m_NoteSpawnerSet[i].SetNoteHitPotision(Quaternion.Euler(0, 0, 45 * i) * m_vFirstHitPosition);
            }
        }
        
    }

    public override void Update()
    {
        //if (!m_PF.GetMusicReady())
        //    return;

        m_fStandbyBeat = m_PF.GetPrepareSongPosInBeats();

        for (int i = 0; i < m_NoteSpawnerSet.Length; i++)
        {

            NoteData tmpNoteData = m_NoteSpawnerSet[i].GetCurrentNoteData();
            if (tmpNoteData == null)
                continue;

            else
            //Debug.Log(tmpNoteData.BeatOfSong.ToString() + "  " + m_StandbyBeat.ToString());

            if (tmpNoteData.BeatOfSong < m_fStandbyBeat)
            {
                //Debug.Log("pre : " +  m_fStandbyBeat.ToString());
                m_NoteSpawnerSet[i].NoteStandby(tmpNoteData.BeatOfSong, m_BeatSystem.GetNoteLiveBeats(), (i + 1).ToString(), m_PF, m_NotePool);
            }
        }
    }

    public override void FixedUpdate()
    {
        //if (!m_PF.GetMusicReady())
        //    return;

        /*
        m_fStandbyBeat = m_PF.GetPrepareSongPosInBeats();

        for (int i = 0; i < m_NoteSpawnerSet.Length; i++)
        {

            NoteData tmpNoteData = m_NoteSpawnerSet[i].GetCurrentNoteData();
            if (tmpNoteData == null)
                continue;

            else
            //Debug.Log(tmpNoteData.BeatOfSong.ToString() + "  " + m_StandbyBeat.ToString());

            if (tmpNoteData.BeatOfSong < m_fStandbyBeat)
            {
                //Debug.Log("pre : " +  m_fStandbyBeat.ToString());
                m_NoteSpawnerSet[i].NoteStandby(tmpNoteData.BeatOfSong, m_BeatSystem.GetNoteLiveBeats() , (i + 1).ToString(), m_PF, m_NotePool);
            }
        }
        */
        
    }

}
