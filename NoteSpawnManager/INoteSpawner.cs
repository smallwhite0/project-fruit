﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class INoteSpawner{

    protected Vector3 m_vNoteSpawnPotision = Vector3.zero;
    protected Vector3 m_vNoteHitPotision = Vector3.zero;

    public void SetNoteSpawnPotision(Vector3 theSpawnPotision)
    {
        m_vNoteSpawnPotision = theSpawnPotision;
    }

    public void SetNoteHitPotision(Vector3 theHitPotision)
    {
        m_vNoteHitPotision = theHitPotision;
    }

    //ublic abstract void NoteStandby(float theBeat, Vector3 theSpawnPotision, string theTag, Project_Fruit PF, NotePool theNotePool);
    public abstract void NoteStandby(float theBeat, float NoteLiveBeats, string theTag, Project_Fruit PF, NotePool theNotePool);
}
