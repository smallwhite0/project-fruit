﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotePool
{
    private static NotePool _instance = null;
    public static NotePool Instance
    {
        get
        {
            if (_instance == null)
                _instance = new NotePool();

            return _instance;

        }
    }

    private Queue<INote> m_NotePool = new Queue<INote>();
    private int m_iPoolDefaultNumber = 30;
    private Sprite[] m_SpriteArray = new Sprite[4];
    private GameObject m_TabsGameObject = null;

    public void Initialize()
    {
        m_TabsGameObject = UnityTool.FindGameObjecct("Tabs");

        NoteType tmpNoteType;
        for (int i = 1; i < (int)NoteType.End; i++)
        {
            tmpNoteType = (NoteType)i;
            m_SpriteArray[i-1] = Resources.Load<Sprite>("Unit/Note/Image/" + tmpNoteType.ToString());
        }

        IUnitFactory theFactory = Project_FruitFactory.GetUnitFactory();

        for (int i = 0; i < m_iPoolDefaultNumber; i++)
        {
            INote tmpNote = theFactory.CreateNote();
            GameObject tmpNoteGameObject = tmpNote.GetGameObject();
            m_NotePool.Enqueue(tmpNote);
            tmpNoteGameObject.transform.SetParent(m_TabsGameObject.transform);
            tmpNote.GetGameObject().SetActive(false);
        }
    }

    public GameObject GetGameObject()
    {
        return m_TabsGameObject;
    }

    public void SetTabsStartPosY(float theStartPosY)
    {
        m_TabsGameObject.transform.position = new Vector3(0, theStartPosY, 0);
    }

    public INote AwakeNoteByNewSets(Vector3 SpawnPosition, Vector3 HitPosition, NoteType theNoteType, string NoteTag, Project_Fruit PF)
    {
        AwakeNoteSettingParam tmpAwakeNoteSettingParam = new AwakeNoteSettingParam();
        tmpAwakeNoteSettingParam.NewUnit = DequeNoteFromPool();
        tmpAwakeNoteSettingParam.noteType = theNoteType;
        tmpAwakeNoteSettingParam.vSpawnPosition = SpawnPosition;
        //tmpAwakeNoteSettingParam.vHitPosition = HitPosition;
        //tmpAwakeNoteSettingParam.fBeatOfNote = BeatOfNote;
        tmpAwakeNoteSettingParam.sNoteTag = NoteTag;

        return Project_FruitFactory.GetUnitFactory().AwakeNote(tmpAwakeNoteSettingParam);
    }

    public INote DequeNoteFromPool()
    {
        INote DequeNote = m_NotePool.Dequeue();
        return DequeNote;
    }

    public void EnqueueNoteToPool(INote RecycleNote)
    {
        m_NotePool.Enqueue(RecycleNote);
        RecycleNote.GetGameObject().SetActive(false);
    }

}
