﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreSystem : IGameSystem
{
    private int m_CurrentScore = 0;
    private int m_GreenNoteHitCount = 0;
    private int m_GreenNoteMissCount = 0;
    private int m_RedNoteHitCount = 0;
    private int m_RedNoteMissCount = 0;
    private int m_HitByGreenCount = 0;
    private int m_HitByRedCount = 0;

    private Text m_ScoreText;

    public ScoreSystem(Project_Fruit PF) : base(PF)
    {
        Initialize();
    }

    public override void Initialize()
    {
        GameObject UIRoot = UITool.FindUIGameObject("TextScoreUI");
        m_ScoreText = UITool.GetUIComponent<Text>(UIRoot, "TextScore");
    }

    public void Reset()
    {
        int m_CurrentScore = 0;
        int m_GreenNoteHitCount = 0;
        int m_GreenNoteMissCount = 0;
        int m_RedNoteHitCount = 0;
        int m_RedNoteMissCount = 0;
        int m_HitByGreenCount = 0;
        int m_HitByRedCount = 0;
    }

    public void PlusGreen()
    {
        m_CurrentScore += 100;
        m_ScoreText.text = m_CurrentScore.ToString();
        m_GreenNoteHitCount += 1;
    }

    public void PlusRed()
    {
        m_CurrentScore += 150;
        m_ScoreText.text = m_CurrentScore.ToString();
        m_RedNoteHitCount += 1;
    }

    public void MissGreen()
    {
        m_GreenNoteMissCount += 1;
    }

    public void MissRed()
    {
        m_RedNoteMissCount += 1;
    }

    public void HitByGreen()
    {
        m_CurrentScore -= 50;
        m_ScoreText.text = m_CurrentScore.ToString();
        m_HitByGreenCount += 1;
    }

    public void HitByRed()
    {
        m_CurrentScore -= 75;
        m_ScoreText.text = m_CurrentScore.ToString();
        m_HitByRedCount += 1;
    }

}
