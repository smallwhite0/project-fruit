﻿//一個Note在pool中被叫醒 -> 組裝 -> 移動 -> 碰撞偵測 -> 回pool

//叫醒在NotePool中的note                   project-fruit / NoteSpawnManager / NoteSpawnSystem.cs
public override void Update()
{
    m_fStandbyBeat = m_PF.GetPrepareSongPosInBeats();

    for (int i = 0; i < m_NoteSpawnerSet.Length; i++)
    {
        NoteData tmpNoteData = m_NoteSpawnerSet[i].GetCurrentNoteData(); //取得下一個要生出的Note資料
        if (tmpNoteData == null)
            continue;

        if (tmpNoteData.BeatOfSong < m_fStandbyBeat) //Note的拍子數小於當前音樂的拍子數加上Note提前出現的拍子數時, 就會從pool中被叫醒
            m_NoteSpawnerSet[i].NoteStandby(tmpNoteData.BeatOfSong, m_BeatSystem.GetNoteLiveBeats(), (i + 1).ToString(), m_PF, m_NotePool);
    }
}

//根據輸入的參數組裝被叫醒的Note           每個步驟的實作放在 project-fruit / Unit / UnitBuilder / AwakeNoteBuilder.cs
public void UnitCreate(IUnitBuilder theBuilder)
{
    theBuilder.LoadAsset();
    theBuilder.SetSprite();
    theBuilder.AddDirectionMark();
    theBuilder.SetTag();
    theBuilder.SetMove();
    theBuilder.SetTrigger();
    theBuilder.SetSoundEffect();
    theBuilder.SetActive();
    theBuilder.AddUnitSystem(m_PF);
}

//Note被叫醒後為譜面物件的child，Note跟隨著譜面物件移動         project-fruit / Move / IMove.cs
private void Update()
{
    m_fCurrentBeat = m_BeatSystem.GetSongPosInBeats(); //獲取當下的拍子數
    float tmpDurationBeats = m_fCurrentBeat - m_fLastBeat; //計算和上一個frame之間差了幾個拍子數
    Vector3 tmpMove = new Vector3(0, m_BeatSystem.CaculatePosMoveY(tmpDurationBeats), 0); //計算這一個frame譜面要移動的距離
    Vector3 tmpNewPos = Vector3.zero;

    if (m_bMoving)
    {
        tmpNewPos = m_vNowPosition - tmpMove;
        m_vNowPosition = tmpNewPos;
        m_TabsGameObject.transform.Translate(-tmpMove); //譜面向下移動
    }

    else //初始微調
    {
        m_vNowPosition = m_TabsGameObject.transform.position - tmpMove;
        m_TabsGameObject.transform.position = m_vNowPosition;
        m_bMoving = true;
    }

    m_fLastBeat = m_fCurrentBeat;
}

//Note偵測到碰撞區後做出反應                     project-fruit / Trigger / UnitTrigger / NoteTrigger.cs
private void FixedUpdate()
{
    CollidionNum = Physics2D.OverlapCircleNonAlloc(transform.position, 0.3f, Colliders, m_iMask);
    if (CollidionNum != 0)
    {
        if (Colliders[0].gameObject.layer == m_iLayer && Colliders[0].gameObject.tag == m_sTag) //碰到相應顏色的碰撞區，可回收flag設為true，並發出回饋聲
        {
            m_Unit.IsKilled();
            m_Unit.UnitGetHit();
            m_bTrigger = true;
        }

        else if (Colliders[0].gameObject.layer == 11) //碰到回收note用的碰撞區
            m_Unit.IsKilled();
    }
}

//Note將components卸除並返回Pool                  project-fruit / Unit / UnitSystem.cs
public void RemoveUnit()
{
    List<IUnit> CanRemoveNotes = new List<IUnit>();

    foreach (INote theNote in m_Notes) //找出正在活動中的Note是否可回收
    {
        if (theNote.IsRemovable())
            CanRemoveNotes.Add(theNote);
    }

    foreach (INote theNote in CanRemoveNotes) //將活動中的Note塞回ObjectPool裡，並從活動中的Note成員中剔除，拔掉Note在組裝過程中裝上的component
    {
        m_NotePool.EnqueueNoteToPool(theNote);
        m_Notes.Remove(theNote);
        theNote.GetOffComponents();
        theNote.GetGameObject().SetActive(false);
    }
}
