﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public struct SelectSongData
{
    public string sSelectedSongName;
    public int iDiffculty;
    public int iGameMode;
}

public class SelectSongBtnData
{
    private GameObject m_ButtonObject;
    private RectTransform m_RectTransform;
    private Button m_Button;
    private Image m_SongIcon;
    private string m_sSongName;

    //private int[] m_iDiffculty = new int[3];

    public SelectSongBtnData(Sprite theSongIcon, string theSongName)
    {
        m_ButtonObject = new GameObject();
        m_RectTransform = m_ButtonObject.AddComponent<RectTransform>();
        m_Button = m_ButtonObject.AddComponent<Button>();
        m_SongIcon = m_ButtonObject.AddComponent<Image>();
        m_SongIcon.sprite = theSongIcon;
        m_sSongName = theSongName;
    }

    public void SetButtonParent(Transform theParent)
    {
        m_ButtonObject.transform.SetParent(theParent);
    }

    public void SetIconPosition(Vector3 newPosition)
    {
        Debug.Log(m_RectTransform.anchoredPosition);
        m_RectTransform.anchoredPosition = newPosition;
        m_RectTransform.sizeDelta = new Vector2(250, 167);
    }

    public Vector3 GetButtonPos()
    {
        return m_ButtonObject.transform.position;
    }

    public void ButtonMovePos(Vector3 newPosition)
    {
        if (m_ButtonObject == null)
            return;

        m_RectTransform.anchoredPosition = newPosition;
    }

    public string GetSongName()
    {
        return m_sSongName;
    }

    public Button GetButton()
    {
        return m_Button;
    }
}


public class SongSelectUICenter : MonoBehaviour
{
    [SerializeField]
    private AnimationCurve MoveCurve;

    private SongSelectState m_SongSelectState;
    //private SelectSongData m_SelectedSongData;
    private GameObject m_UIRoot;

    private float m_fMenuSpeed = 2.5f;
    private Vector3 m_vCenter = new Vector3(0.0f, 0.0f, 0.0f);
    private int m_iCurrentIndex = 0;
    private int m_iPreIndex = 0;
    private float m_fProcessing = 1f;
    private float m_fInterval = 280.0f;

    private SelectSongBtnData[] m_MenuListButton;

    public void SongSelectUIInitial(SongSelectState theSongSelectState)
    {
        m_SongSelectState = theSongSelectState;
        m_UIRoot = UITool.FindUIGameObject("SongSelectUI");
        string SongDirPath = Application.dataPath + "/Resources/Song";
        DirectoryInfo SongDir = new DirectoryInfo(SongDirPath);
        DirectoryInfo[] DiChild = SongDir.GetDirectories();
        m_MenuListButton = new SelectSongBtnData[DiChild.Length];
        Sprite tmpSongIcon;

        for (int i = 0; i < DiChild.Length; i++)
        {
            tmpSongIcon = Resources.Load<Sprite>("Song/" + DiChild[i].Name + "/cover");
            m_MenuListButton[i] = new SelectSongBtnData(tmpSongIcon, DiChild[i].Name);
            m_MenuListButton[i].SetButtonParent(m_UIRoot.transform);
            m_MenuListButton[i].SetIconPosition(Vector3.zero + (new Vector3(m_fInterval, 0.0f, 0.0f) * i));
        }

        //tmpButton.GetButton().onClick.AddListener(() => OnSongSelectButtonClick(tmpButton));
    }

    public void SongSelectUIUpdate()
    {
        m_fProcessing += Time.deltaTime * m_fMenuSpeed;

        if (Input.GetKeyDown(KeyCode.Return))
            m_SongSelectState.OnSongSelectButtonClick(m_MenuListButton[m_iCurrentIndex]);

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            m_fProcessing = 0.0f;
            m_iPreIndex = m_iCurrentIndex;
            m_iCurrentIndex++;
            //tmpButton.ButtonMovePos(tmpButton.GetButtonPos() + Vector3.left);
        }


        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            m_fProcessing = 0.0f;
            m_iPreIndex = m_iCurrentIndex;
            m_iCurrentIndex--;
            //tmpButton.ButtonMovePos(tmpButton.GetButtonPos() + Vector3.right);
        }

        if (m_fProcessing < 1.0f)
        {
            for (int i = 0; i < m_MenuListButton.Length; i++)
                m_MenuListButton[i].ButtonMovePos(Vector3.Lerp(m_vCenter + new Vector3((0 - m_iPreIndex) * m_fInterval, 0.0f, 0.0f), m_vCenter + new Vector3((0 - m_iCurrentIndex) * m_fInterval, 0.0f, 0.0f), MoveCurve.Evaluate(m_fProcessing)));

        }
    }
}
