﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IUserInterface{

    protected Project_Fruit m_PF = null;
    protected GameObject m_RootUI = null;
    private bool m_bActive = true;
    public IUserInterface(Project_Fruit PF)
    {
        m_PF = PF;
    }

    public bool IsVisible()
    {
        return m_bActive;
    }

    public void Show()
    {
        m_RootUI.SetActive(true);
        m_bActive = true;
    }

    public void Hide()
    {
        m_RootUI.SetActive(false);
        m_bActive = false;
    }

    public virtual void Initialize() { }
    public virtual void Release() { }
    public virtual void Update() { }

}
