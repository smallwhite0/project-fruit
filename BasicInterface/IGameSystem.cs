﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IGameSystem{

    protected Project_Fruit m_PF = null;
    public IGameSystem(Project_Fruit PF)
    {
        m_PF = PF;
    }

    public virtual void Initialize() { }
    public virtual void Release() { }
    public virtual void Update() { }
    public virtual void FixedUpdate() { }
    public virtual void ApplicationFocus(bool focus) { }
    public virtual void ApplicationPause(bool pause) { }
}
