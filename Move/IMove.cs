﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IMove : MonoBehaviour {

    protected IUnit m_Unit;
    protected float m_fUnitRealProcess;
    protected float m_fUnitMoveProcess = 0f;
    protected Vector3 m_vUnitStartPosition;
    protected Transform m_tUnitTransform;

    public void SetUnit(IUnit theUnit)
    {
        m_Unit = theUnit;
        m_tUnitTransform = m_Unit.GetTransform();
    }

    public void SetPosition(Vector3 theStartPosition)
    {
        m_vUnitStartPosition = theStartPosition;
    }

    public virtual void ReleaseFromUnit() { }
}

public class TabsMove : IMove
{
    private static BeatSystem m_BeatSystem = null;
    private GameObject m_TabsGameObject = null;
    private bool m_bMoving = false;
    private float m_fLastBeat;
    private float m_fCurrentBeat;
    private float m_fNoteSpeed;
    private Vector3 m_vNowPosition;
    private Vector3 m_vUnitHitPosition;
    private Rigidbody2D m_Rigidbody2D;

    private double m_fSongPosition;
    private double m_fPreSongPosition;

    private float m_fCurrentTotalBeat = 0f;
    private float m_fCurrentTotalMove = 0f;


    public static void SetBeatSystem(BeatSystem theBeatSystem)
    {
        m_BeatSystem = theBeatSystem;
    }

    public void SetGameObject(GameObject theGameObject)
    {
        m_TabsGameObject = theGameObject;
    }

    public void SetLastBeat(float theBeat)
    {
        m_fLastBeat = theBeat;
    }

    public void SetNotePosition()
    {
        m_vUnitStartPosition = m_BeatSystem.GetSpawnPos();
        m_vUnitHitPosition = m_BeatSystem.GetHitPos();
        m_Rigidbody2D = transform.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        m_fCurrentBeat = m_BeatSystem.GetSongPosInBeats();
        //m_fSongPosition = m_BeatSystem.GetSongPosition();
        float tmpDurationBeats = m_fCurrentBeat - m_fLastBeat;
        Vector3 tmpMove = new Vector3(0, m_BeatSystem.CaculatePosMoveY(tmpDurationBeats), 0);
        Vector3 tmpNewPos = Vector3.zero;
        m_fCurrentTotalMove += tmpMove.y;
        m_fCurrentTotalBeat += tmpDurationBeats;

        //Debug.LogFormat("Now : {0},  Last : {1}", m_fCurrentBeat, m_fLastBeat);

        if (m_bMoving)
        {
            tmpNewPos = m_vNowPosition - tmpMove;
            m_vNowPosition = tmpNewPos;
            m_TabsGameObject.transform.Translate(-tmpMove);

            //Debug.LogFormat("Pre : {0}", m_vNowPosition.y);
            //Debug.LogFormat("Move : {0}", tmpMove.y);
            //Debug.LogFormat("TotalMove : {0}   TotalBeat : {1}", m_fCurrentTotalMove, m_fCurrentTotalBeat);
            //Debug.LogFormat("Now beats: {0}  TotalBeats: {1}  TotalMove: {2}", m_fCurrentBeat, m_fCurrentTotalBeat, m_fCurrentTotalMove);
        }

        else //初始微調
        {
            m_vNowPosition = m_TabsGameObject.transform.position - tmpMove;
            m_TabsGameObject.transform.position = m_vNowPosition;
            m_bMoving = true;   
        }

        /*
        double Duration = m_fSongPosition - m_fPreSongPosition;
        string latency = Duration > 0.005 ? "no" : "yes";

        if (Duration < 0.005)
            Debug.LogFormat("Now beats: {0}   Now position: {1}   Pre position: {2}  Duration : {3}  {4}", m_fCurrentBeat, m_fSongPosition, m_fPreSongPosition, Duration, latency);
        */

        //Debug.LogFormat("Now beats: {0}   Now position: {1}   Pre position: {2}  Duration : {3}  NowPos : {4} {5}", m_fCurrentBeat, m_fSongPosition, m_fPreSongPosition, Duration, tmpMove.y, latency);

        m_fLastBeat = m_fCurrentBeat;
        //m_fPreSongPosition = m_fSongPosition;
    }
    
}
